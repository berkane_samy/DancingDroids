use std::io;
use rand::Rng;
use std::process::Command;
use std::{thread, time};

enum Orientation{
North,
Sud,
West,
East
}
enum Instruction{
L,
F,
R
}

struct Robot {
    id : i32,
    x: i32,
    y :i32,
    orient : Orientation,
    instru :   Vec<Instruction> 
}

fn deplacement_r ( r : &mut Robot , a: i32, i: usize,  tab :&mut Vec<i32>,score :  &mut i32)
{
    


 match r.instru [i] {
      Instruction::L => {
        r.orient = match  r.orient {
            Orientation::North => Orientation::West ,
            Orientation::East => Orientation::North ,
            Orientation::West => Orientation::Sud,
            Orientation::Sud  => Orientation::East,
        } ;
    
    }

    Instruction::R => {
        r.orient = match  r.orient {
         Orientation::North => Orientation::East ,
         Orientation::East => Orientation::Sud ,
         Orientation::West => Orientation::North ,
         Orientation::Sud  => Orientation::West,
        } ;
    }
    
        Instruction::F => {
                match  r.orient {
             Orientation::North => {
                    
                    
                    if r.y == a
                    {if tab[(r.x) as usize] == 0
                        {   tab[(r.y*(a+1) + r.x) as usize ] = 0;
                            tab[ (r.x) as usize ] = r.id;
                            r.y = 0;
                        }else{
                            println!("Robot ID<{}> Collision avec Robot ID<{}> en  ({},{})",r.id,tab[(r.x) as usize],r.x,0);
                            *score = 1;
                        }
                       
                    }else{if tab[((r.y+1)*(a+1)+r.x) as usize] == 0
                        {   tab[(r.y*(a+1) + r.x) as usize ] = 0;
                            tab [((r.y+1)*(a+1)+r.x) as usize]  = r.id;
                            r.y +=1;
                        }else{
                            *score = 1;
                            println!("Robot ID<{}> Collision avec Robot ID<{}> en  ({},{})",r.id,tab[((r.y+1)*(a+1)+r.x) as usize],r.x,r.y+1);
                        }
                        
                    }
                } 
             Orientation::East => {
                 
                if r.x == a
                {if tab[(r.y*(a+1)) as usize] == 0
                    {   tab[(r.y*(a+1) + r.x ) as usize] = 0;
                        tab[ (r.y*(a+1)) as usize] = r.id;
                        r.x = 0;
                    }else{
                        *score = 1;
                        println!("Robot ID<{}> Collision avec Robot ID<{}> en  ({},{})",r.id,tab[(r.y*(a+1)) as usize],0,r.y);
                    }
                    
                }else{if tab[((r.y)*(a+1)+r.x+1) as usize] == 0
                    {   tab[(r.y*(a+1) + r.x ) as usize] = 0;
                        tab [((r.y)*(a+1)+r.x+1) as usize]  = r.id;
                        r.x +=1;
                    }else{
                        *score = 1;
                        println!("Robot ID<{}> Collision avec Robot ID<{}> en  ({},{})",r.id,tab[((r.y)*(a+1)+r.x+1) as usize],r.x+1,r.y);
                    }
                   
                }
             } 
             Orientation::West => {
                if r.x == 0
                {if tab[(r.y*(a+1)+a) as usize] == 0
                    {   tab[(r.y*(a+1) + r.x ) as usize] = 0;
                        tab [(r.y*(a+1)+a) as usize]  = r.id;
                        r.x = a;
                    }else{
                        *score = 1;
                        println!("Robot ID<{}> Collision avec Robot ID<{}> en  ({},{})",r.id,tab[(r.y*(a+1)+a) as usize],0,r.y);
                    }
                    
                }else{if tab[((r.y)*(a+1)+r.x-1) as usize] == 0
                    {   tab[(r.y*(a+1) + r.x) as usize ] = 0;
                        tab [((r.y)*(a+1)+r.x-1) as usize]  = r.id;
                        r.x -=1;
                    }else{
                        *score = 1;
                        println!("Robot ID<{}> Collision avec Robot ID<{}> en  ({},{})",r.id,tab[((r.y)*(a+1)+r.x-1) as usize],r.x-1,r.y);
                    }
                    
                }
             }
                Orientation::Sud => {
                    if r.y == 0
                    {if tab[(a*(a+1)+ r.x) as usize] == 0
                        {   tab[(r.y*(a+1) + r.x ) as usize] = 0;
                            tab[(a*(a+1)+ r.x) as usize] = r.id;
                            r.y = a;
                        }else{
                            *score = 1;
                            println!("Robot ID<{}> Collision avec Robot ID<{}> en  ({},{})",r.id,tab[(a*(a+1)+ r.x) as usize],r.x,0);
                        }
                        
                    }else{if tab[((r.y-1)*(a+1)+r.x) as usize] == 0
                        {   tab[(r.y*(a+1) + r.x) as usize ] = 0;
                            tab [((r.y-1)*(a+1)+r.x) as usize]  = r.id;
                            r.y -=1;
                        }else{
                            *score = 1;
                            println!("Robot ID<{}> Collision avec Robot ID<{}> en  ({},{})",r.id,tab[((r.y-1)*(a+1)+r.x) as usize],r.x,r.y+1);
                        }
                        
                    }

                }
            } 
    
        }
    }
}
 


fn lecture_char()->String 
{   
    loop{

    println!("saisir un orientation :");
    let mut instruction = String::new();
    
    io::stdin().read_line(&mut instruction).expect("vous n'avez pas saisie de chaine de char");
    instruction =  instruction.trim().to_string();
   
    if instruction == "L"|| instruction == "F" || instruction == "R" || instruction == "l"|| instruction == "f" || instruction == "r"
    { 
      return instruction;
    }

    println!("   Commande introuvable ");

    
    }

}
fn deplacement_02 ( r : &mut Robot ,a: i32, s: String , tab :& mut Vec <i32>)->bool
{   let mut inst = Instruction::L;
    let s1 = "L".to_string() ;
    if s == s1 {inst = Instruction::L}
    let s1 = "R".to_string() ;
    if s == s1 {inst = Instruction::R}
    let s1 = "F".to_string() ;
    if s == s1 {inst = Instruction::F}
    let s1 = "l".to_string() ;
    if s == s1 {inst = Instruction::L}
    let s1 = "r".to_string() ;
    if s == s1 {inst = Instruction::R}
    let s1 = "f".to_string() ;
    if s == s1 {inst = Instruction::F}
    let mut  score = 0;
    while r.instru.len() > 0
    {
        r.instru.pop();
    }
    r.instru.push(inst);
    deplacement_r ( r  , a, 0,  tab , &mut score );
    r.instru.pop();
    return score == 1;


}
fn affichage_02(v :  &mut Vec <Robot> ,  a :  i32)
{ println!("                    ==========================");
println!(" ");

   
   println!(" ");
for i in 0 .. a
{   print!("                   ");
     print!(" {} ",a-1-i);

   for j in 0 .. a
   {let mut h = false;
    for r in v.iter()
    { 
    if r.x == j && r.y == (a-1-i)
    {   h = true;
        let fleche = match r.orient{
            
            Orientation::North =>'↑',
            Orientation::East=>'→',
            Orientation::West =>'←',
            Orientation::Sud=>'↓',
           
        };
        print!(" {:?} ",fleche);
    }
    }if h == false  
    {
        print!("  ■  ");
    }

    
   }   
println!(" ");
println!(" ");
} 
println!("                        0    1    2    3    4");
}


fn battel_de_robot(vb :  &mut Vec <Robot>,mut tab :& mut Vec <i32>, a : i32)
{   
    let mut score : Vec<i32> = Vec::new();
    for _k in 0 .. vb.len()
    {
        score.push(0)
    }
  
   let mut rng = rand::thread_rng();
   
    loop
    {   effacer_terminal();
        affichage_02(vb ,  a );
          for  i in 0 .. vb.len()
        {  let mut sc = 0;
            let mut choix  = rng.gen::<i32>()%3;
            
            if choix < 0
            {
                choix = -choix;
            }
            if choix == 0
            {
                vb[i].instru.push(Instruction::F)
            }
            if choix == 1
            {
                vb[i].instru.push(Instruction::L)
            }
            if choix == 2
            {
                vb[i].instru.push(Instruction::R)
            }
            
            deplacement_r(  &mut vb[i]  , a-1, 0,&mut tab,&mut sc);
            vb[i].instru.pop();
            
              println!(" ");
            
            
            if sc == 1
            {
                
                let ten_millis = time::Duration::from_millis(3000);
                thread::sleep(ten_millis);
                  let output = Command::new("clear").output().unwrap_or_else(|e| {
                panic!("failed to execute process: {}", e)
              });
              println!("{}", String::from_utf8_lossy(&output.stdout));
            }
            score[i] = score[i] + sc;
            
        }
        for k in 0 .. score.len()
                {
                  print!("           ROBOT {}   =  {}         ",k,score[k]);
              }
        
    
        
        let ten_millis = time::Duration::from_millis(500);
        thread::sleep(ten_millis);
        let output = Command::new("clear").output().unwrap_or_else(|e| {
            panic!("failed to execute process: {}", e)
          });
        
          println!("{}", String::from_utf8_lossy(&output.stdout));
        

    }  
}
fn menu()
{
println!("                               =================================");
println!("                              //                             // ");
println!("                             //    welcome to dancig droid  // ");
println!("                            //                             // ");
println!("                           =================================");
println!("           ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
println!("           ::                                                                    ::");
println!("           ::   1)  to dance with a random robot press 1                         ::");
println!("           ::                                                                    ::");
println!("           ::   2)  to see a random coregraphie of robot press 2                 ::");
println!("           ::                                                                    ::");
println!("           ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
}
fn user_robot()
{   let a = 5;
     let  r_user = Robot{
        id : 0,
        x:0,
        y:0,
        orient : Orientation::East,
        instru  :vec![]
    };
    let  r_boot = Robot{
        id : 1,
        x:3,
        y:3,
        orient : Orientation::West,
        instru  :vec![]
    };
    let mut tab : Vec<i32> = Vec::new();
    for _i in 0 .. a*a
    {
        tab.push(0);
    }
    tab[(r_user.y * 5 + r_user.x) as usize] = r_user.id;
    tab[(r_user.y * 5 + r_user.x) as usize] = r_user.id;
    
    
    let mut  score_user = 0;
    let mut score_boot = 0;
    let mut rng = rand::thread_rng();
    let mut v: Vec<Robot> = Vec::new();
    v.push(r_user);
    v.push(r_boot);
    loop
    {
    

    effacer_terminal();
    println!(" your score :      {}          enemi score  :     {}",score_user,score_boot);
    affichage_02(&mut v  , 5);
   
  
        let s  = lecture_char();
        let b = deplacement_02 ( &mut v[0] ,4, s ,& mut tab);
        if b 
        {
            score_user = score_user+1;
            let ten_millis = time::Duration::from_millis(3000);
            thread::sleep(ten_millis);
              let output = Command::new("clear").output().unwrap_or_else(|e| {
            panic!("failed to execute process: {}", e)
          });
        
          println!("{}", String::from_utf8_lossy(&output.stdout));
        }
        effacer_terminal();
        println!(" your score :      {}          enemi score  :     {}",score_user,score_boot);
        affichage_02(&mut v  , 5);
        let mut choix  = rng.gen::<i32>()%3;
            
            if choix < 0
            {
                choix = -choix;
            }
        let mut instr = "L";
        if choix == 0
        {
            instr = "R";
        }
        if choix == 1
        {
            instr = "F";
        }
        let b = deplacement_02 ( &mut v[1] ,4, instr.to_string() ,& mut tab);
        if b 
        {
            score_boot = score_boot+1;
            let ten_millis = time::Duration::from_millis(3000);
            thread::sleep(ten_millis);
              let output = Command::new("clear").output().unwrap_or_else(|e| {
            panic!("failed to execute process: {}", e)
          });
          println!("{}", String::from_utf8_lossy(&output.stdout));
        }
        effacer_terminal();
        println!(" your score :      {}          enemi score  :     {}",score_user,score_boot);
        affichage_02(&mut v  , 5);

    }
}
fn effacer_terminal()->()
{
    let output = Command::new("clear").output().unwrap_or_else(|e| {
        panic!("failed to execute process: {}", e)
      });
    
      println!("{}", String::from_utf8_lossy(&output.stdout));
}
fn random_dance()->()
{
    let  r1 = Robot{
        id : 1,
        x:3,
        y:1,
        orient : Orientation::West,
        instru  :vec![]
    };
    let  r2 = Robot{
        id : 2,
        x:1,
        y:1,
        orient : Orientation::East,
        instru  :vec![]
    };
    let  r3 = Robot{
        id : 3,
        x:0,
        y:0,
        orient : Orientation::East,
        instru  :vec![]
    };
    let a = 5;
    let mut tab : Vec<i32> = Vec::new();
    for _i in 0 .. a*a
    {
       tab.push(0);
    }
    tab[(r1.y * 5 + r1.x) as usize] = r1.id;
    tab[(r2.y * 5 + r2.x) as usize] = r2.id;
    tab[(r3.y * 5 + r3.x) as usize] = r3.id;
    let mut v: Vec<Robot> = Vec::new();
    v.push(r1);
    v.push(r2);
    v.push(r3);
    battel_de_robot(&mut v , &mut tab , 5);
}
fn main() {
    
   
     

    effacer_terminal();
    menu();
    loop{
    println!("                          your choice  :");
    let mut choix = String::new();
    
    io::stdin().read_line(&mut  choix).expect("vous n'avez pas saisie de chaine de char");
    choix=   choix.trim().to_string();
    if (choix == "1"    || choix == "2") && choix.len() == 1
    {
        if choix == "1"
        {
            user_robot();
        }
        if choix == "2"
        {
            random_dance();
        }
    }
    }
  

}
